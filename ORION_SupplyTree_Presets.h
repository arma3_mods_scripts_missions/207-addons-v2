class ORION_SupplyTree_Presets 
{
    class ReSupplyTree 
    {
        displayName = "Base Resupply Tree";
        objects[] = {"_resupply_tree"};
        
    };

    class SectionBoxTree 
    {
        displayName = "Section boxes Tree";
        objects[] = {"_resupply_tree", "_section_boxes"};
    };
    
    class MedicalBoxTree 
    {
        displayName = "Medical Boxes Tree";
        objects[] = {"_resupply_tree", "_medical_box"};
    };

    class MortarsDemoTree
    {
        displayName = "Mortas and Demo Tree";
        objects[] = {"_resupply_tree", "_mortars_demo"};
    };
    class MissionSpecificTree
    {
        displayName = "Mission Specific Tree";
        objects[] = {"_resupply_tree", "_mission_specific"};
    };
};