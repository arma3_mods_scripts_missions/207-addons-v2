class Cfg3DEN {
    class Attributes {
        class Default;
        class Title: Default {
            class Controls {
                class Title;
            };
        };
        class Combo: Title {
           class Controls: Controls {
               class Title: Title {};
               class Value;
           };
        };
        class ORION_ValueSelection: Combo {
            class Controls: Controls {
                class Title: Title {};
                class Value: Value {
                    delete Items;
                    class ItemsConfig {
                        path[] = {"ORION_SupplyTree_Presets"};
                        localConfig = 0;
                        propertyText = "displayName";
                        sort = 0;
                    };
                };
            };
        };
    };
};
