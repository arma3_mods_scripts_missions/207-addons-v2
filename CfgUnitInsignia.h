class CfgUnitInsignia
{
    class ORION_0_A_MC
    {
        displayName = "0-A MC";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\0_a_mc.paa";
        textureVehicle = "";
    };

    class ORION_1_1_Alpha
    {
        displayName = "1-1 Alpha";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_1_alpha.paa";
        textureVehicle = "";
    };

    class ORION_1_1_Bravo
    {
        displayName = "1-1 Bravo";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_1_bravo.paa";
        textureVehicle = "";
    };

    class ORION_1_1_Bravo_Boomerang
    {
        displayName = "1-1 Bravo Boomerang";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_1_bravo_boomerang.paa";
        textureVehicle = "";
    };

    class ORION_1_1_Charlie
    {
        displayName = "1-1 Charlie";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_1_charlie.paa";
        textureVehicle = "";
    };

    class ORION_1_COY_HQ
    {
        displayName = "1-1 COY HQ";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_coy_hq.paa";
        textureVehicle = "";
    };

    class ORION_1_TROOP_HQ
    {
        displayName = "1-1 Troop HQ";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_troop_hq.paa";
        textureVehicle = "";
    };

    class ORION_1_5_Gauntlet
    {
        displayName = "1-5 Gauntlet";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_5_gauntlet.paa";
        textureVehicle = "";
    };

    class ORION_1_5_Hawkeye
    {
        displayName = "1-5 Hawkeye";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_5_hawkeye.paa";
        textureVehicle = "";
    };

    class ORION_1_5_Molar
    {
        displayName = "1-5 Foxtrot";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_5_molar.paa";
        textureVehicle = "";
    };

    class ORION_1_5_Nightmare
    {
        displayName = "1-5 Nightmare";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\1_5_nightmare.paa";
        textureVehicle = "";
    };

    class ORION_207_Logo
    {
        displayName = "207 Logo";
        author = "207";
        texture = "\ORION\Mods\207_Addons\patches\207_logo.png";
        textureVehicle = "";        
    };
};