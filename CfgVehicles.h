class CBA_Extended_EventHandlers;

class CfgVehicles
{
    class Logic;
    class Module_F: Logic
    {
        class AttributesBase
        {
            class Default;
            class Edit;
            class Combo;
            class Checkbox;
            class CheckboxNumber;
            class ModuleDescription;
            class Units;
        };

        class ModuleDescription
        {
            class AnyVehicle;
            class AnyStaticObject;
        };
    };

   class ORION_Supply207V2_Main_Module: Module_F
    {
        scope = 2;
        displayName = "207 Supply Main Module";
        category = "ORION_Supply_207_Faction_Class";
        // icon = "\ORION\Mods\207_Addons\ui\supply_main_module_ca.paa"; 

        function = "ORION_supply_scripts_fnc_mainModule207";
        functionPriority = 1;
        isGlobal = 1;
        isTriggerActivated = 1;
        is3Den = 0;
        isDisposable = 0;

        class Attributes: AttributesBase
        {
            class UnloadAllItems: Checkbox
            {
                displayName = "ACE Interact, unload all items";
                property = "ORION_Supply207V2_Main_Module_UnloadAllItems";
                typeName = "BOOL";
                defaultValue = true;
            };

            class BeretsCheckbox: Checkbox
            {
                displayName = "Berets Checkbox";
                property = "ORION_Supply207V2_Main_Module_BeretsCheckbox";
                typeName = "BOOL";
                defaultValue = true;
            };

            class Berets: Edit
            {
                displayName = "Berets";
                property = "ORION_Supply207V2_Main_Module_Berets";
                typeName = "ARRAY";
                defaultValue = '[["V_207_Beret", 60], ["O_207_Beret", 60], ["J_207_Beret", 60]]';
            };

            class PalletJack: Checkbox
            {
                displayName = "Pallet Jack";
                property = "ORION_Supply207V2_Main_Module_PalletJack";
                typeName = "BOOL";
                defaultValue = true;
            };

            class BoxLoader: Checkbox
            {
                displayName = "Extra OPDEM Crates";
                property = "ORION_Supply207V2_Main_Module_BoxLoader";
                typeName = "BOOL";
                defaultValue = true;
            };
        };

        class ModuleDescription: ModuleDescription
        {
            description = "207 Supply Scripts V2 allows the mission maker to choose which boxes and their contents, and define the ACE tree for the action(s)";
            sync[] = {"LocationArea_F"};

            class LocationArea_F
            {
                description[] = {"207 Supply Script V2 (Do not use)"};
                position = 1;
                direction = 1;
                optional = 0;
                duplicate = 0;
                synced[] = {""};
            };
        };
    };

    class ORION_Supply207V2_Module: Module_F
    {
        scope = 2;
        displayName = "207 Supply Box Module";
        category = "ORION_Supply_207_Faction_Class";
        // icon = "\ORION\Mods\207_Addons\ui\supply_box_module_ca.paa";

        function = "ORION_supply_scripts_fnc_supplyCall207";
        functionPriority = 1;
        isGlobal = 1;
        isTriggerActivated = 1;
        is3Den = 0;
        isDisposable = 0;

        class Attributes: AttributesBase
        {
            class EnableBox: Checkbox
            {
                displayName = "Enable Box";
                property = "ORION_Supply207V2_Module_EnableBox";
                typeName = "BOOL";
                defaultValue = true;
            };

            class BoxType: Edit
            {
                displayName = "Class name for Box";
                property = "ORION_Supply207V2_Module_BoxType";
                typeName = "STRING";
                tooltip = "Class name for the box. Can be obtained by using 'typeOf _this' on the object or hovering over object in EDEN Editor";
                defaultValue = 'Box_NATO_Ammo_F';
            };

            class BoxContents: Edit
            {
                displayName = "Box Contents";
                tooltip = "Contents of the box in an array of arrays with each individual array being a class name for item and number of specific items";
                property = "ORION_Supply207V2_Module_BoxContents";
                typeName = "ARRAY";
                defaultValue = '[["rhs_mag_30Rnd_556x45_Mk318_Stanag_Pull", 20],["1Rnd_HE_Grenade_shell", 10],["SMA_30Rnd_68x43_BT_IR", 20],["ACE_20Rnd_762x51_Mk316_Mod_0_Mag", 15],["SMA_20Rnd_762x51mm_Mk316_Mod_0_Special_Long_Range_IR", 15]]';
            };

            class BoxSize: Edit
            {
                displayName = "Box ACE Cargo Size";
                property = "ORION_Supply207V2_Module_BoxSize";
                tooltip = "How much ACE cargo space to be taken by the box";
                typeName = "NUMBER";
                defaultValue = 3;
            };  

            class BoxSpace: Edit
            {
                displayName = "Box ACE Cargo space";
                property = "ORION_Supply207V2_Module_BoxSpace";
                tooltip = "Size of ACE cargo for the box";
                typeName = "NUMBER";
                defaultValue = 12;
            };

            class AceTree: Edit
            {
                displayName = "ACE action and display name list.";
                property = "ORION_Supply207V2_Module_AceTree";
                tooltip = "ACE action name and display name for the box in a list. Action name is to be separated with underscore with letters only, display name can have any ASCII character.";
                typeName = "ARRAY";
                defaultValue = '["small_arms", "Small Arms Box"]';
            };

            class SupplyTreeSection: Default
            {
                displayName = "Choose Supply Tree";
                property = "ORION_Supply20V2_Module_SupplyTreeSection";
                tooltip = "Choose which tree should the box go under";
                control = "ORION_ValueSelection";
                typeName = "STRING";
                defaultValue = 0;
            };
        };

        class ModuleDescription: ModuleDescription
        {
            description = "207 Supply Scripts V2 allows the mission maker to choose which boxes and their contents, and define the ACE tree for the action(s)";
            sync[] = {"LocationArea_F"};

            class LocationArea_F
            {
                description[] = {"207 Supply Script V2 (Do not use)"};
                position = 1;
                direction = 1;
                optional = 0;
                duplicate = 0;
                synced[] = {""};
            };
        };
    };

    class ORION_Fortify207_Module: Module_F
    {
        scope = 2;
        displayName = "207 Fortify Module (Do not use)";
        category = "ORION_Supply_207_Faction_Class";
        // icon = "\ORION\Mods\207_Addons\ui\fortify_module_ca.paa";

        function = "ORION_fortify_fnc_fortifymodule207";
        functionPriority = 1;
        isGlobal = 1;
        isTriggerActivated = 1;
        is3Den = 0;
        isDisposable = 0;

        class Attributes: AttributesBase
        {
            class PlayerCost: Edit
            {
                displayName = "Money allocated";
                property = "ORION_Fortify207_Module_PlayerCost";
                typeName = "INTEGER";
                defaultValue = 200;
            };

            class FortifyObjects: Edit
            {
                displayName = "List Fortify Objects with cost";
                property = "ORION_Fortify207_Module_FortifyObjects";
                typeName = "ARRAY";
                defaultValue = '[["Land_BagFence_Long_F", 5], ["Land_BagBunker_Small_F", 50]]';
            };
        };

        class ModuleDescription: ModuleDescription
        {
            description = "207 Fortify module which changes how much in-game money any player has for fortification, each player can have a different cost associated with it";
            sync[] = {"LocationArea_F"};

            class LocationArea_F
            {
                description[] = {"207 Fortify Module (Do not use)"};
                position = 1;
                direction = 1;
                optional = 0;
                duplicate = 0;
                synced[] = {""};
            };
        };
    };

    class Air;
    class Helicopter: Air 
    {
        class ACE_SelfActions
        {
            class ORION_paraDropCargp
            {
                displayName = "Paradrop all cargo";
                condition = "ASLToATL getPosASL _target select 2 > 30";
                exceptions[] = {};
                statement = "[_target, _player] call ORION_support_element_fnc_paraDropCargo207";
                icon = "";
            };
        };

        class ACE_Actions
        {
            class ACE_MainActions
            {
                class ORION_unLoadCargo
                {
                    displayName = "Unload All Cargo";
                    condition = "true";
                    exceptions[] = {};
                    statement = "[_target, _player] call ORION_support_element_fnc_unLoadAllItems207";
                    distance = 2;
                    position = "[_target, eyePos _player] call ace_interaction_fnc_getVehiclePosComplex";
                };
            };
        };
    };
};
