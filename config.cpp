#define _ARMA_

class cfgPatches
{
	class 207_Beret
	{
		author="Tim";
		name="Beret [Detachment 207]";
		url="";
		requiredVersion=0.1;
		units[]=
		{
		
		};
		weapons[] = 
		{
			"V_207_Beret",
			"O_207_Beret",
			"J_207_Beret",
			"A_207_Beret"
		};
	};

	class Intro_207
	{
		author = "Orion";
		requiredAddons[] = {"A3_Data_F_Enoch_Loadorder"};
		requiredVersion = 1.0;
		units[] = {};
		weapons[] = {};
	};

	class ORION_Supply_207
	{
		units[] = {"ORION_Supply207_Module"};
		requiredVersion = 2.0;
		requiredAddons[] = {"A3_Modules_F"};
	};
};

// Beret for 207
class cfgWeapons
{
	class H_Beret_02;
	class HeadgearItem;

	// Veteran Operator Beret
	class V_207_Beret: H_Beret_02
	{
		author = "Tim";
		scope=2;
		weaponPoolAvailable = 1;
		scopeCurator=2;
		displayName="Veteran Beret [Detachment 207]";
		picture = "\ORION\Mods\207_Addons\ui\207beret";
		model = "A3\characters_f_epb\BLUFOR\headgear_beret02.p3d";
		hiddenSelections[] = {"Camo"};
		hiddenSelectionsTextures[] = {"ORION\Mods\207_Addons\data\headgear_207beret_co.paa"};
		editorCategory="EdCat_Equipment";
		editorSubcategory="EdSubcat_Hats";
		class ItemInfo: HeadgearItem {
			mass = 1;
			uniformModel = "A3\characters_f_epb\BLUFOR\headgear_beret02.p3d";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"ORION\Mods\207_Addons\data\headgear_207beret_co.paa"};
			
		};
	};

	// Operator Beret
	class O_207_Beret: H_Beret_02
	{
		author = "Orion";
		scope=2;
		weaponPoolAvailable = 1;
		scopeCurator=2;
		displayName="Operator Beret [Detachment 207]";
		picture = "\ORION\Mods\207_Addons\ui\207beret";
		model = "A3\characters_f_epb\BLUFOR\headgear_beret02.p3d";
		hiddenSelections[] = {"Camo"};
		hiddenSelectionsTextures[] = {"ORION\Mods\207_Addons\data\o_headgear_207beret_co.paa"};
		editorCategory="EdCat_Equipment";
		editorSubcategory="EdSubcat_Hats";
		class ItemInfo: HeadgearItem {
			mass = 1;
			uniformModel = "A3\characters_f_epb\BLUFOR\headgear_beret02.p3d";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"ORION\Mods\207_Addons\data\o_headgear_207beret_co.paa"};
		};
	};

	// Junior Operator Beret
	class J_207_Beret: H_Beret_02
	{
		author = "Orion";
		scope=2;
		weaponPoolAvailable = 1;
		scopeCurator=2;
		displayName="Junior Operator Beret [Detachment 207]";
		picture = "\ORION\Mods\207_Addons\ui\207beret";
		model = "A3\characters_f_epb\BLUFOR\headgear_beret02.p3d";
		hiddenSelections[] = {"Camo"};
		hiddenSelectionsTextures[] = {"ORION\Mods\207_Addons\data\j_headgear_207beret_co.paa"};
		editorCategory="EdCat_Equipment";
		editorSubcategory="EdSubcat_Hats";
		class ItemInfo: HeadgearItem {
			mass = 1;
			uniformModel = "A3\characters_f_epb\BLUFOR\headgear_beret02.p3d";
			hiddenSelections[] = {"Camo"};
			hiddenSelectionsTextures[] = {"ORION\Mods\207_Addons\data\j_headgear_207beret_co.paa"};
		};
	};
};

// Custom Menu Screen
class CAWorld;
class RscStandardDisplay;
class RscVignette;
class RscControlsGroupNoScrollbars;
class RscFrame;
class RscText;
class RscPicture;
class RscTitle;
class RscButtonMenu;
class RscHTML;
class RscControlsGroupNoHScrollbars;
class RscListBox;
class RscPictureKeepAspect;
class RscButton;
class RscButtonImages;
class RscShortcutButton;
class RscDisplayConfigure
{
	enableDisplay = 1;
};

class RscDisplayMain: RscStandardDisplay
{
	idd = 0;
	idc = 1;
	access = 1;
	enableDisplay = 1;
	text = "\ORION\Mods\207_Addons\data\207_menu.paa";
	class RscActiveText;
	class RscActivePicture: RscActiveText
	{
		style = 48;
		color[] = {1,1,1,0.5};
		colorActive[] = {1,1,1,1};
	};

	class Spotlight{};
	class controls
	{
		class Spotlight1{};
		class Spotlight2{};
		class Spotlight3{};
		class BackgroundSpotlightRight{};
		class BackgroundSpotlightLeft{};
		class BackgroundSpotlight{};
		class B_Credits{};

		class ConnectOpServer2302: RscButton
		{
			idc = -1;
			text = "Join Detachment 207 Operation Server Port 2302";
			style = 2;
			onbuttonclick = "connectToServer ['103.193.80.51', 2302, '220077']";
			colorBackground[] = {1,0,0,0.2};
			borderSize = 0.055;
			colorBorder[] = {0,0,0,0};
			x = "safeZoneX + safeZoneW - 0.8";
			y = "safeZoneY + 0.2 * safeZoneH";
			w = "0.50";
			h = "0.05";
		};

		class ConnectOpServer2402: RscButton
		{
			idc = -1;
			text = "Join Detachment 207 Operation Server Port 2402";
			style = 2;
			onbuttonclick = "connectToServer ['103.193.80.51', 2402, '220077']";
			colorBackground[] = {1,0,0,0.2};
			borderSize = 0.055;
			colorBorder[] = {0,0,0,0};
			x = "safeZoneX + safeZoneW - 0.8";
			y = "safeZoneY + 0.25 * safeZoneH";
			w = "0.50";
			h = "0.05";
		};

		class ConnectTrainingServer: RscButton
		{
			idc = -1;
			text = "Join Detachment 207 Training Server";
			style = 2;
			onbuttonclick = "connectToServer ['103.193.80.51', 2402, '220077']";
			colorBackground[] = {1,0,0,0.2};
			borderSize = 0.055;
			colorBorder[] = {0,0,0,0};
			x = "safeZoneX + 0.2 * safeZoneW";
			y = "safeZoneY + 0.2 * safeZoneH";
			w = "0.50";
			h = "0.05";
		};

		class ConnectPublicServer: RscButton
		{
			idc = -1;
			text = "Join Detachment 207 Public Server";
			style = 2;
			onbuttonclick = "connectToServer ['103.193.80.51', 2350, 'doorkicking207']";
			colorBackground[] = {1,0,0,0.2};
			borderSize = 0.055;
			colorBorder[] = {0,0,0,0};
			x = "safeZoneX + 0.2 * safeZoneW";
			y = "safeZoneY + 0.25 * safeZoneH";
			w = "0.50";
			h = "0.05";
		};
	};
	class controlsBackground
	{
		class LoadingPicture_207: RscPicture
		{
			idc = -1;
			access = 2;
			colorText[] = {1,1,1,1};
			x = "SafeZoneX";
			y = "SafeZoneY";
			w = "SafeZoneW";
			h = "SafeZoneH";
			text = "\ORION\Mods\207_Addons\data\207_menu.paa";
		};
	};
};

class CfgFactionClasses
{
	class NO_CATEGORY;
	class ORION_Supply_207_Faction_Class: NO_CATEGORY
	{
		displayName = "207 Modules Category";
	};
};

// 207 functions
#include "CfgFunctions.h"

// #3DEN functions
#include "Cfg3DEN.h"
#include "ORION_SupplyTree_Presets.h"

// Module creation
#include "CfgVehicles.h"

// Unit Insignia
#include "CfgUnitInsignia.h"

class CfgMods
{
	name = "207 Addons (v2)";
	author = "Orion, Tim";
};