class CfgFunctions
{
   
    class ORION_Fortify
    {
        tag = "ORION_fortify";
        class FortifyCall
        {   
            class fortifymodule207 { file = "\ORION\Mods\207_Addons\functions\fortifyobjects\fnc_fortifyModule.sqf";};
        };

        class ForitfyAction
        {
            class fortifyAction207 { file = "\ORION\Mods\207_Addons\functions\fortifyobjects\fnc_addfortifyAction.sqf";};
        };

        class DisplayNames
        {
            class displayNames207 { file = "\ORION\Mods\207_Addons\functions\fortifyobjects\fnc_displayNames.sqf";};
        };

        class SpawnObjects
        {
            class spawnObjects207 { file = "ORION\Mods\207_Addons\functions\fortifyobjects\fnc_spawnObject.sqf";};
        };
    };

    class ORION_SupportElement
    {
        tag = "ORION_support_element";
        class ORION_UnLoadAllItems
        {
            class unLoadAllItems207 { file = "\ORION\Mods\207_Addons\functions\supportelement\fnc_unLoadAllItems.sqf";};
        };

        class ORION_ParaDropCargo
        {
            class paraDropCargo207 { file = "\ORION\Mods\207_Addons\functions\supportelement\fnc_paraDropCargo.sqf";};
        };
    };

    class ORION_SupplyV2
    {
        tag = "ORION_supply_scripts";
        class MainModule
        {
            class mainModule207 { file = "\ORION\Mods\207_Addons\functions\supplyscriptsv2\fnc_mainModule.sqf";};
        };

        class MainActions
        {
            class mainActions207 { file = "\ORION\Mods\207_Addons\functions\supplyscriptsv2\fnc_mainActions.sqf";};
        };

        class SupplyCall
        {
            class supplyCall207 { file = "\ORION\Mods\207_Addons\functions\supplyscriptsv2\fnc_supplyCall.sqf";};
        };

        class CreateBox
        {
            class createBox207 { file = "\ORION\Mods\207_Addons\functions\supplyscriptsv2\fnc_createBox.sqf";};
        };

        class CreateTree
        {
            class createTree207 { file = "\ORION\Mods\207_Addons\functions\supplyscriptsv2\fnc_createTree.sqf";};
        };

        class BoxLoader
        {
            class boxLoader207 { file = "\ORION\Mods\207_Addons\functions\supplyscriptsv2\fnc_boxloader.sqf";};
        };

        class PalletJack
        {
            class palletJack207 { file = "\ORION\Mods\207_Addons\functions\supplyscriptsv2\fnc_palletJack.sqf";};
        };
    };
};