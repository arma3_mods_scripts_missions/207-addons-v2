// ORION_supply_scripts_fnc_createBox207
params ["_target", "_player", "_params"];

_box_type = _params select 0;
_box_items = parseSimpleArray (_params select 1);
_box_size = _params select 2;
_box_space = _params select 3;
_box_display_name = _params select 4;

_player_position = getPosATL _target;

_crate = createVehicle [_box_type, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;
[_crate, _box_size] call ace_cargo_fnc_setSize;
[_crate, _box_space] call ace_cargo_fnc_setSpace;

clearWeaponCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearBackpackCargo _crate;

{_crate addItemCargoGlobal [_x select 0, _x select 1]} foreach _box_items;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

_hint_text = str _box_display_name + " spawned";

hintSilent parseText _hint_text;