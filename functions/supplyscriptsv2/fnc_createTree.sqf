// ORION_supply_scripts_fnc_createTree207

_logic = param [0, objNull, [objNull]];
_resupply_box = param [1];
_activated = param [2, true, [true]];

_enable_supply_box = _logic getVariable "EnableBox";
_supply_box_type = _logic getVariable "BoxType";
_supply_box_contents = _logic getVariable "BoxContents";
_supply_box_size = _logic getVariable "BoxSize";
_supply_box_space = _logic getVariable "BoxSpace";
_supply_box_ace_names = parseSimpleArray (_logic getVariable "AceTree");

_supply_box_tree_preset = _logic getVariable "SupplyTreeSection";
_supply_box_supply_tree = getArray (configFile >> "ORION_SupplyTree_Presets" >> _supply_box_tree_preset >> "objects");

_resupply_box_name = str _resupply_box;
_supply_box_ace_tree = ["ACE_MainActions"];

_supply_box_supply_tree apply {_action = [_resupply_box_name + _x]; _supply_box_ace_tree append _action;};

// systemChat str _supply_box_ace_tree;

/*
ACE interaction cheat sheet

* Argument:
 * 0: Action name <STRING>
 * 1: Name of the action shown in the menu <STRING>
 * 2: Icon <STRING>
 * 3: Statement <CODE>
 * 4: Condition <CODE>
 * 5: Insert children code <CODE> (Optional)
 * 6: Action parameters <ANY> (Optional)
 * 7: Position (Position array, Position code or Selection Name) <ARRAY>, <CODE> or <STRING> (Optional)
 * 8: Distance <NUMBER> (Optional)
 * 9: Other parameters [showDisabled,enableInside,canCollapse,runOnHover,doNotCheckLOS] <ARRAY> (Optional)
 * 10: Modifier function <CODE> (Optional)

_resup = ["resupply_tree", "Choose Resupply Crate", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions"], _resup] call ace_interact_menu_fnc_addActionToObject;
*/

_check_main_module_variable = _resupply_box_name + "_main_module_init";
_fail_text = "Main Module not initialised for " + _resupply_box_name + " object.";
// if !(missionNamespace getVariable [_check_main_module_variable, false]) exitWith {hintSilent _fail_text};


_action_name         = _supply_box_ace_names select 0;
_action_display_name = _supply_box_ace_names select 1;

_action = [_action_name, _action_display_name, "", ORION_supply_scripts_fnc_createBox207, {true}, {}, [_supply_box_type, _supply_box_contents, _supply_box_size, _supply_box_space, _action_display_name]] call ace_interact_menu_fnc_createAction;
[_resupply_box, 0, _supply_box_ace_tree, _action] call ace_interact_menu_fnc_addActionToObject;
 