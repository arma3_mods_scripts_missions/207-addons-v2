// ORION_supply_scripts_fnc_mainModule207

_logic = param [0, objNull, [objNull]];
_units = param [1, [], [[]]];
_activated = param [2, true, [true]];

if !(_activated) exitWith {};
if (isNull _logic) exitWith {};
    
{ [_logic, _x, _activated] call ORION_supply_scripts_fnc_mainActions207} forEach _units;