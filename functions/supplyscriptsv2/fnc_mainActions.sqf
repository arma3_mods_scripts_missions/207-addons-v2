// ORION_supply_scripts_fnc_mainActions207

_logic = param [0, objNull, [objNull]];
_resupply_box = param [1];
_activated = param [2, true, [true]];

[_resupply_box, 25] call ace_cargo_fnc_setSpace;

_unload_all_items_checkbox = _logic getVariable "UnloadAllItems";

_berets_checkbox = _logic getVariable "BeretsCheckbox";
_berets_array = _logic getVariable "Berets";

_pallet_jack_checkbox = _logic getVariable "PalletJack";
_boxloader_checkbox = _logic getVariable "BoxLoader";

/*
ACE interaction cheat sheet

* Argument:
 * 0: Action name <STRING>
 * 1: Name of the action shown in the menu <STRING>
 * 2: Icon <STRING>
 * 3: Statement <CODE>
 * 4: Condition <CODE>
 * 5: Insert children code <CODE> (Optional)
 * 6: Action parameters <ANY> (Optional)
 * 7: Position (Position array, Position code or Selection Name) <ARRAY>, <CODE> or <STRING> (Optional)
 * 8: Distance <NUMBER> (Optional)
 * 9: Other parameters [showDisabled,enableInside,canCollapse,runOnHover,doNotCheckLOS] <ARRAY> (Optional)
 * 10: Modifier function <CODE> (Optional)

_resup = ["resupply_tree", "Choose Resupply Crate", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[resupply, 0, ["ACE_MainActions"], _resup] call ace_interact_menu_fnc_addActionToObject;
*/

_main_module_init = str _resupply_box + "_main_module_init";
_resup_action_name = str _resupply_box + "_resupply_tree";
_resup = [_resup_action_name, "Choose Resupply Crate", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[_resupply_box, 0, ["ACE_MainActions"], _resup] call ace_interact_menu_fnc_addActionToObject;

// Miscellaneous Actions
_misc_stuff = ["misc_stuff", "Misc Stuff", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[_resupply_box, 0 ,["ACE_MainActions", _resup_action_name], _misc_stuff] call ace_interact_menu_fnc_addActionToObject;

// Boxloader Pallet Jack Spawn from crate
if (_pallet_jack_checkbox) then {
    _pallet_jack = ["pallet_jack", "<t color='#4FA64F'>Pallet Jack</t>", "", ORION_supply_scripts_fnc_palletJack207, {true}] call ace_interact_menu_fnc_createAction;
    [_resupply_box, 0, ["ACE_MainActions", _resup_action_name, "misc_stuff"], _pallet_jack] call ace_interact_menu_fnc_addActionToObject;
};

// OPDEM box spawn from crate
if (_boxloader_checkbox) then {
    _boxloader = ["boxloader", "<t color='#4FA64F'>Boxloader</t>", "", ORION_supply_scripts_fnc_boxLoader207, {true}] call ace_interact_menu_fnc_createAction;
    [_resupply_box, 0, ["ACE_MainActions", _resup_action_name, "misc_stuff"], _boxloader] call ace_interact_menu_fnc_addActionToObject;
};

// Berets box from crate
if (_berets_checkbox) then {
    _berets = ["berets", "<t color='#4FA64F'>Berets</t>", "", ORION_supply_scripts_fnc_createBox207, {true}, {}, 
    ["Land_WoodenCrate_01_F", _berets_array, 1, 12, "Berets"]] call ace_interact_menu_fnc_createAction;
    [_resupply_box, 0, ["ACE_MainActions", _resup_action_name, "misc_stuff"], _berets] call ace_interact_menu_fnc_addActionToObject;
};

// Unload all items at once
if (_unload_all_items_checkbox) then {
    _unload_all_items = ["unload_all_items", "Unload All items", "", ORION_support_element_fnc_unLoadAllItems207, {true}] call ace_interact_menu_fnc_createAction;
    [_resupply_box, 0, ["ACE_MainActions"], _unload_all_items] call ace_interact_menu_fnc_addActionToObject;
};

// Creating the ACE sub-section tree

// Section boxes 
_squad_boxes_action_name = str _resupply_box + "_section_boxes";
_squad_boxes = [_squad_boxes_action_name, "Squad Boxes", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[_resupply_box, 0, ["ACE_MainActions", _resup_action_name], _squad_boxes] call ace_interact_menu_fnc_addActionToObject;

// Medical Boxes
_medical_section_action_name = str _resupply_box + "_medical_box";
_medical_section = [_medical_section_action_name, "Medical", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[_resupply_box, 0, ["ACE_MainActions", _resup_action_name], _medical_section] call ace_interact_menu_fnc_addActionToObject;

// Heavy Arms
_heavy_arms_section_action_name = str _resupply_box + "_heavy_arms";
_heavy_arms_section = [_heavy_arms_section_action_name, "Heavy Arms", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[_resupply_box, 0, ["ACE_MainActions", _resup_action_name], _heavy_arms_section] call ace_interact_menu_fnc_addActionToObject;

// Mortars and Explosives
_mortars_demo_section_action_name = str _resupply_box + "_mortars_demo";
_mortars_demo_section = [_mortars_demo_section_action_name, "Mortas and Demo", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[_resupply_box, 0, ["ACE_MainActions", _resup_action_name], _mortars_demo_section] call ace_interact_menu_fnc_addActionToObject;

// Mission Specific
_mission_specific_section_action_name = str _resupply_box + "_mission_specific";
_mission_specific_section = [_mission_specific_section_action_name, "Mission Specific", "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[_resupply_box, 0, ["ACE_MainActions", _resup_action_name], _mission_specific_section] call ace_interact_menu_fnc_addActionToObject;