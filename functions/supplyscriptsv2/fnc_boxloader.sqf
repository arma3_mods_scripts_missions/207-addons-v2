params ["_target", "_player", "_params"];

_target = _this select 0;
_player = _this select 1;

_player_position = getPosATL _target;

_crate_classname = "Land_Boxloader_Crate_1";
_crate = createVehicle [_crate_classname, _player_position, [], 0.5, "CAN_COLLIDE"];
_crate allowDamage false;

[_crate, 12] call ace_cargo_fnc_setSpace;
[_crate, 2] call ace_cargo_fnc_setSize;

clearWeaponCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearBackpackCargo _crate;

_test = [_crate, _target] call ace_cargo_fnc_loadItem;

hintSilent parseText "Boxloader Crate spawned";