params ["_target", "_player", "_params"];

_name = _params select 0;
_cost = _params select 1;
_class_name = _params select 2;

_player_budget = profileNamespace getVariable _name;
if (typeName _player_budget != "SCALAR") then {
    _player_budget = parseNumber _player_budget;
};

[_target, _player, [west, _class_name]] call ace_fortify_fnc_deployObject;

_deploy_finish = _target getVariable "ace_fortify_deployFinished";
_deploy_cancel = _target getVariable "ace_fortify_deployCanceled";

_player_budget = _player_budget - _cost;

profileNamespace setVariable [_name, _player_budget];