_logic = param [0, objNull, [objNull]];
_unit = param [1];

_player_cost =  _logic getVariable "PlayerCost";
_fortify_objects = parseSimpleArray (_logic getVariable "FortifyObjects");
if (count _fortify_objects == 0) exitWith {systemChat "Empty list for objects placements"};

_player_name = profileName;

ORION_globalVariable = "Hello World";

_objects_list_name = _player_name + "_objects_list";
_objects_list = [];

// Setting profileNamespace varaibles for the player defining total cost and variable names for objects placed
profileNamespace setVariable [_player_name, _player_cost];
profileNamespace setVariable [_objects_list_name, _objects_list];

_fortify_condition = [_unit, "ACE_Fortify"] call BIS_fnc_hasItem;

/*
ACE interaction cheat sheet

* Argument:
 * 0: Action name <STRING>
 * 1: Name of the action shown in the menu <STRING>
 * 2: Icon <STRING>
 * 3: Statement <CODE>
 * 4: Condition <CODE>
 * 5: Insert children code <CODE> (Optional)
 * 6: Action parameters <ANY> (Optional)
 * 7: Position (Position array, Position code or Selection Name) <ARRAY>, <CODE> or <STRING> (Optional)
 * 8: Distance <NUMBER> (Optional)
 * 9: Other parameters [showDisabled,enableInside,canCollapse,runOnHover,doNotCheckLOS] <ARRAY> (Optional)
 * 10: Modifier function <CODE> (Optional)
[0 : "GiveItems", 1: "?", 2: "",3: _statement, 4: _condition, 5: _insertChildren, 6: [123], 7: "",8: 4,9: [false, false, false, true, false], 10: _modifierFunc]
*/

// Starting the tree
_fortify_main_display_name = "207 Fortify $" + (profileNamespace getVariable _player_name);
_fortify_tree = ["fortify_tree", _fortify_main_display_name, "", {nil}, {true}] call ace_interact_menu_fnc_createAction;
[_unit, 1, ["ACE_SelfActions"], _fortify_tree] call ace_interact_menu_fnc_addActionToObject;

// Adding interactions in a for loop for each class of object which can be deployed
{   
    _object_class_name   = _x select 0;
    _object_cost         = _x select 1;
    _object_display_name = [_object_class_name] call ORION_fortify_fnc_displayNames207;


    _fortify_ace_action_name  = _object_class_name;
    _fortify_ace_display_name = _object_display_name + " Cost: $" + str _object_cost;

    _fortify_action = [_fortify_ace_action_name, 
                       _fortify_ace_display_name, "", ORION_fortify_fnc_spawnObjects207, {true}, {}, 
                       [_player_name, _object_cost, _object_class_name]] call ace_interact_menu_fnc_createAction;

    [_unit, 1, ["ACE_SelfActions", "fortify_tree"], _fortify_action] call ace_interact_menu_fnc_addActionToObject;
} forEach _fortify_objects;