params ["_class_name"];

_class_name = _this select 0;

_cfgName = "";

switch true do
{
    case(isClass(configFile >> "CfgMagazines" >> _class_name)): {_cfgName = "CfgMagazines"};
    case(isClass(configFile >> "CfgWeapons" >> _class_name)): {_cfgName = "CfgWeapons"};
    case(isClass(configFile >> "CfgVehicles" >> _class_name)): {_cfgName = "CfgVehicles"};
    case(isClass(configFile >> "CfgGlasses" >> _class_name)): {_cfgName = "CfgGlasses"};
    case(isClass(configFile >> "CfgAmmo" >> _class_name)): {_cfgName = "CfgAmmo"};
};

_class_name_display_name = getText(configFile >> _cfgName >> _class_name >> "displayName");

_class_name_display_name