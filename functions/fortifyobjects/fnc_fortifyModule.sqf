_logic = param [0, objNull, [objNull]];
_units = param [1, [], [[]]];
_activated = param [2, true, [true]];

if !(_activated) exitWith {};
if (isNull _logic) exitWith {};

{ [_logic, _x] call ORION_fortify_fnc_fortifyAction207} forEach _units;