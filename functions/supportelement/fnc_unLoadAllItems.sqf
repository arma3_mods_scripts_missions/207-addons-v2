params ["_target", "_player"];

_target = _this select 0;

ORION_cargo_space_left = [_target] call ace_cargo_fnc_getCargoSpaceLeft;

_all_items = _target getVariable ["ace_cargo_loaded", []];
_all_items_count = count _all_items;

_player_name = profileNameSteam;
_name_space_variable = _player_name + "_item_count_unloadAllItems";
profileNamespace setVariable [_name_space_variable, _all_items_count];

[{(_this select 0) params ["_cargo_array", "_target", "_onFinish", "_profileVariable"]; 
    _index = profileNamespace getVariable _profileVariable;
    _size_item = [_cargo_array select _index] call ace_cargo_fnc_getSizeItem;
    ORION_cargo_space_left = ORION_cargo_space_left + _size_item;
    [_cargo_array select _index, _target] call ace_cargo_fnc_unloadItem;
    _index = _index - 1;
    profileNamespace setVariable [_profileVariable, _index];
    if (_index <= _onFinish) then {[_this select 1] call CBA_fnc_removePerFrameHandler;}; 
    }, 0.1, [_all_items, _target, -1, _name_space_variable]] call CBA_fnc_addPerFrameHandler;

[_target, 25] call ace_cargo_fnc_setSpace;

hint "All items are removed from vehicle";