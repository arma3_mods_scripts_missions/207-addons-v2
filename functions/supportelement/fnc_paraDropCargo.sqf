params ["_target", "_player"];

_target = _this select 0;

_all_items = _target getVariable ["ace_cargo_loaded", []];
_all_items_count = count _all_items;
_player_name = profileNameSteam;
_name_space_variable = _player_name + "_item_count";
profileNamespace setVariable [_name_space_variable, _all_items_count];


// {_time = time; [_x, _target] call ace_cargo_fnc_paradropItem; waitUntil {time > (_time + 1.5)};} foreach _all_items;

// _all_items_check = count (_target getVariable ["ace_cargo_loaded", []]);

// if (_all_items_check != 0) then {
//     systemChat "Items are still left in the ACE cargo";
//     _left_items = _target getVariable ["ace_cargo_loaded", []];
//     {[_x, _target] call ace_cargo_fnc_paradropItem} foreach _left_items;
// };

[{(_this select 0) params ["_cargo_array", "_target", "_onFinish", "_profileVariable"]; 
    _index = profileNamespace getVariable _profileVariable; 
    [_cargo_array select _index, _target] call ace_cargo_fnc_paradropItem;
    _index = _index - 1;
    profileNamespace setVariable [_profileVariable, _index];
    if (_index <= _onFinish) then {[_this select 1] call CBA_fnc_removePerFrameHandler;}; 
    }, 0.5, [_all_items, _target, -1, _name_space_variable]] call CBA_fnc_addPerFrameHandler;

[_target, 25] call ace_cargo_fnc_setSpace;

hint "All items are paradropped from helicopter";